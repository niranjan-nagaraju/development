#ifndef __SLL_PRINT_H_
#define __SLL_PRINT_H_

/** Print operations */
void sll_print (sll_t *sll, void(*printfn)(void *));

#endif
